# Term project for BI-XML, 2017/2018

Full score.
You should probably only use this as a reference - it would be kinda... obvous.
The individual datasets were shortened to 4999 lines.
Licensed under MIT except for the datasets themselves.

## How to run the generating script:

  1. Install [BetterSubBox](https://gitlab.com/mikulaspoul/BetterSubBox) locally
  2. Log in there and get some data in there by logging in
  3. Update ``YOUTUBE_API_KEY`` in ``get_videos.py`` with a key.
  4. Run [Django Shell](https://docs.djangoproject.com/en/2.0/ref/django-admin/#shell)
  5. Import the individual functions and run them.

## Documentation:

The LaTeX file works on [ShareLaTeX](http://sharelatex.com).

## Assignment (in Czech):

Semestrální práce může být na libovolné téma, důležité je dodržet následující požadavky.

 * Požadavky
   * vybrat si min. 3 různé datasety (možno i vygenerovat smysluplná data náhodně)
 * každý vstupní dataset bude mít nejméně 5.000 záznamů (pokud budete předvádět v učebně, zvolte data tak, aby bylo časově reálné úlohu ukázat)
 * tyto (min.) 3 datasety naimportovat do databáze
 * vytvořit nový dataset, který bude agregovat data z jednoho původního datasetu
 * vytvořit nový dataset, který bude agregovat data ze dvou původních datasetů najednou
 * vytvořit nad kterýmkoliv datasetem index a připravit 3 různé dotazy do tohoto indexu (nestačí index databáze, je potřeba použít indexovací engine jako je Solr či Elastic)
   * využit filtrování
   * využít třídění
   * použít wildcard hledání (www.*soft*.com)
 * jako technologii lze použít kteroukoliv distribuovanou noSQL databázi a nad ní postavený index
   * doporučené technologie jsou Cassandra, Hive, Solr (či Elasticsearch)
 * k semestrální práci je potřeba zpracovat kompletní dokumentaci (odevzdat ve formátu PDF), která bude obsahovat minimálně:
   * klasickou strukturu včetně hlavičky, rejstříku, úvodu, hlavní části, závěru
   * popis databáze (businessový pohled na to, jaká data se budou používat)
   * popis a ukázku dat z použitých datasetů
   * kompletní příkazy použité pro jednotlivé transformace
   * popis použitých technologií (high-level, jaké nástroje jsou použity)
   * v příloze použité konfigurační soubory (např. pro search engine)
   * na základě dokumentace by mělo být možné se vstupními datasety kompletně replikovat vaší práci!
