import csv
from uuid import uuid4

import requests
import dateutil.parser
from dateutil.relativedelta import relativedelta

import numpy.random
from django.db.models import Q
from django.utils import timezone
from apps.channel.models import Channel
from apps.video.models import Video
from pathlib import Path

YOUTUBE_API_KEY = "---YOUTUBE-API-KEY---"


def get_videos():
    q = Q()

    for channel in Channel.objects.all():
        q |= Q(pk__in=channel.videos.filter(published__isnull=False).order_by("-published")[:10])

    return Video.objects.filter(q).select_related("channel")


def get_videos_csv():
    videos = get_videos()

    with Path("videos.csv").open("w") as fl:
        writer = csv.writer(fl, delimiter=";")
        writer.writerow(
            ["YouTube ID", "Title", "Published", "Channel ID", "Channel Name"]
        )

        for video in videos:
            writer.writerow(
                [video.youtube_id, video.title, video.published.isoformat(),
                 video.channel.youtube_id, video.channel.title]
            )


def get_all_comments_for_video(video):
    query = {
        'key': YOUTUBE_API_KEY,
        'part': 'id,snippet',
        'maxResults': 50,
        'textFormat': 'plainText',
        'videoId': video.youtube_id
    }

    response = requests.get(
        'https://www.googleapis.com/youtube/v3/commentThreads',
        params=query
    )
    response = response.json()

    comments = []

    while True:
        if "items" not in response:
            break

        for item in response["items"]:
            comments.append(item["snippet"]["topLevelComment"])

        break
        # if "nextPageToken" not in response:
        #     break
        #
        # query["pageToken"] = response["nextPageToken"]
        #
        # response = requests.get(
        #     'https://www.googleapis.com/youtube/v3/commentThreads',
        #     params=query
        # )
        # response = response.json()

    return comments


def get_comments_csv():
    with Path("comments.csv").open("w") as fl:
        writer = csv.writer(fl, delimiter=";")

        writer.writerow(
            ["Comment ID", "Video ID", "Timestamp", "Author", "Text"]
        )

        i = 0
        for video in get_videos():


            comments = get_all_comments_for_video(video)
            for comment in comments:
                writer.writerow([
                    comment["id"],
                    comment["snippet"]["videoId"],
                    dateutil.parser.parse(comment["snippet"]["updatedAt"]).isoformat(),
                    comment["snippet"]["authorDisplayName"],
                    comment["snippet"]["textDisplay"].replace("\n", "\\n")
                ])
            i += 1

            if i % 10 == 0:
                print(i)


def get_counts_for_video(video):
    response = requests.get(
        'https://www.googleapis.com/youtube/v3/videos',
        params={
            "key": YOUTUBE_API_KEY,
            "id": video.youtube_id,
            "part": "statistics"
        }
    )
    response = response.json()

    try:
        likes = int(response["items"][0]["statistics"]["likeCount"])
        dislikes = int(response["items"][0]["statistics"]["dislikeCount"])

        if likes > 100000:
            return likes // 10000, dislikes // 10000
        elif likes > 10000:
            return likes // 1000, dislikes // 1000
        elif likes > 1000:
            return likes // 100, dislikes // 100
        elif likes > 100:
            return likes // 10, dislikes // 10
        else:
            return likes, dislikes
    except:
        raise ValueError


def generate_ratings(video, cnt, rating, writer):
    left = 0
    middle = left + 24 * 60 * 60
    if video.published + relativedelta(days=1) > timezone.now():
        right = middle
    else:
        right = timezone.now().timestamp() - video.published.timestamp()

    x = numpy.random.triangular(left, middle, right, cnt)

    for y in x:
        writer.writerow(
            [str(uuid4()).replace("-", ""),
             video.youtube_id,
             (video.published + relativedelta(seconds=y)).isoformat(),
             rating]
        )


def get_likes_csv():
    with Path("likes.csv").open("w") as fl:
        writer = csv.writer(fl, delimiter=";")

        writer.writerow(
            ["Rating ID", "Video ID", "Timestamp", "Rating"]
        )

        for video in get_videos():
            try:
                likes, dislikes = get_counts_for_video(video)
            except ValueError:
                continue

            generate_ratings(video, likes, "Liked", writer)
            generate_ratings(video, dislikes, "Disliked", writer)
